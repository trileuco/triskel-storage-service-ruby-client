# frozen_string_literal: true

require 'test/unit'
require 'webmock/test_unit'
require_relative '../lib/triskel_storage_service_ruby_client'

module TriskelStorage
  BASE_URI = 'http://example.com'
  STORAGE = "#{BASE_URI}/#{HTTPStorage::STORAGE_ENDPOINT}"
  METADATA = "#{BASE_URI}/#{HTTPStorage::METADATA_ENDPOINT}"
  UUID = 'A1234'
  NAME = 'tux.png'
  LABELS = 'logo,client,app'
  TYPE = 'image/png'
  R_SIZE = '23000'
  DATA = 'ABC1234'
  REQUEST_LABELS = %w[logo client app].freeze
  RAW_LABELS = 'logo,client,app'
  SIZE = 23_000
  MD5_SUM = 'caf464a2c3387bb41847f120f4a84c53'
  UPLOAD_DATE = Time.parse('2019-06-02T14:56:30.586Z')
  UPLOAD_DATE_R = '2019-06-02T14:56:30.586Z'
  CACHE_CONTROL = 'public, max-age=31536000'

  class ClientCases < Test::Unit::TestCase
    def test_upload_file
      stub_request(:post, STORAGE).with(body: DATA, headers:
          stub_upload_headers).to_return(status: 201)
      client = HTTPStorage.new(BASE_URI)
      client.save(file_fixture)
    end

    def test_retrieve_file_correctly
      stub_request(:get, "#{STORAGE}/#{UUID}")
        .to_return(status: 200, body: DATA, headers: stub_response_headers)
      client = HTTPStorage.new(BASE_URI)
      file = client.retrieve(UUID)
      assert_retrieved_file(file)
    end

    def test_not_found_error
      stub_request(:get, "#{STORAGE}/#{UUID}")
        .to_return(status: 404,
                   headers: default_headers,
                   body: not_found_body)
      client = HTTPStorage.new(BASE_URI)
      assert_raises(FileNotFoundError) do
        client.retrieve(UUID)
      end
    end

    def test_m_not_found_error
      stub_request(:get, "#{METADATA}/#{UUID}")
        .to_return(status: 404,
                   headers: default_headers,
                   body: not_found_body)
      client = HTTPStorage.new(BASE_URI)
      assert_raises(FileNotFoundError) do
        client.find_by_uuid(UUID)
      end
    end

    def test_delete_by_uuid
      stub_request(:delete, "#{STORAGE}/#{UUID}")
        .to_return(status: 200)
      client = HTTPStorage.new(BASE_URI)
      client.delete_by_uuid(UUID)
    end

    def test_find_by_uuid
      stub_request(:get, "#{METADATA}/#{UUID}")
        .to_return(status: 200,
                   headers: default_headers,
                   body: stub_response_body)
      client = HTTPStorage.new(BASE_URI)
      assert_file_metadata(client.find_by_uuid(UUID))
    end

    def test_find_by_labels
      stub_request(:get, "#{METADATA}?labels=triskel&labels=app")
        .to_return(status: 200,
                   headers: default_headers,
                   body: stub_label_response_body)
      client = HTTPStorage.new(BASE_URI)
      results = client.find_by_labels(%w[triskel app])
      results.each(&method(:assert_file_metadata))
    end

    def test_find_by_labels_empty
      stub_request(:get, "#{METADATA}?labels=triskel&labels=app")
        .to_return(status: 200,
                   headers: default_headers,
                   body: '[]')
      client = HTTPStorage.new(BASE_URI)
      results = client.find_by_labels(%w[triskel app])
      assert_equal([], results)
    end

    private

    def default_headers
      { 'Content-Type' => 'application/json' }
    end

    def assert_retrieved_file(file)
      assert_equal(UUID, file.uuid)
      assert_equal(NAME, file.name)
      assert_equal(REQUEST_LABELS, file.labels)
      assert_equal(TYPE, file.mime_type)
      assert_equal(R_SIZE, file.size)
      assert_equal(DATA, file.data)
      assert_equal(MD5_SUM, file.md5_sum)
      assert_equal(UPLOAD_DATE, file.upload_date)
      assert_equal(CACHE_CONTROL, file.cache_control)
    end

    def stub_response_body
      ::File.read('test/stub_response_body.json')
    end

    def stub_label_response_body
      '[' + stub_response_body + ',' + stub_response_body + ']'
    end

    def assert_file_metadata(file)
      assert_equal(UUID, file.uuid)
      assert_equal(NAME, file.name)
      assert_equal(REQUEST_LABELS, file.labels)
      assert_equal(TYPE, file.mime_type)
      assert_equal(SIZE, file.size)
      assert_equal(nil, file.data)
      assert_equal(MD5_SUM, file.md5_sum)
      assert_equal(UPLOAD_DATE, file.upload_date)
      assert_equal(CACHE_CONTROL, file.cache_control)
    end

    def stub_upload_headers
      {
        Mapper::HEADER_FILE_UUID => UUID,
        Mapper::HEADER_FILE_NAME => NAME,
        Mapper::HEADER_FILE_LABELS => RAW_LABELS,
        Mapper::HEADER_FILE_MIME_TYPE => TYPE,
        Mapper::HEADER_FILE_SIZE => SIZE,
        Mapper::HEADER_FILE_CACHE_CONTROL => CACHE_CONTROL
      }
    end

    def stub_response_headers
      {
        Mapper::HEADER_FILE_UUID => UUID,
        Mapper::HEADER_FILE_NAME => NAME,
        Mapper::HEADER_FILE_LABELS => RAW_LABELS,
        Mapper::HEADER_FILE_MIME_TYPE => TYPE,
        Mapper::HEADER_FILE_MD5_SUM => MD5_SUM,
        Mapper::HEADER_FILE_SIZE => SIZE,
        Mapper::HEADER_FILE_UPLOAD_DATE => UPLOAD_DATE_R,
        Mapper::HEADER_FILE_CACHE_CONTROL => CACHE_CONTROL
      }
    end

    # @@return [TriskelStorage::File] file
    def file_fixture
      file = TriskelStorage::File.new
      file.uuid = UUID
      file.name = NAME
      file.data = DATA
      file.labels = REQUEST_LABELS
      file.mime_type = TYPE
      file.size = SIZE
      file.cache_control = CACHE_CONTROL
      file
    end

    def not_found_body
      '{"message":"cannot find file A1234"}'
    end
  end
end
