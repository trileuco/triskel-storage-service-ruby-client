# frozen_string_literal: true

source 'https://rubygems.org'

gem 'overcommit', '~> 0.47.0'
gem 'rubocop', '~> 0.81.0'
