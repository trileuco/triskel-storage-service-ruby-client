# frozen_string_literal: true

require 'time'

module TriskelStorage
  # Mapper class for ensuring public interface is respected
  class Mapper
    HEADER_FILE_UUID = 'Storage-Service-File-Uuid'
    HEADER_FILE_NAME = 'Storage-Service-File-Name'
    HEADER_FILE_LABELS = 'Storage-Service-File-Labels'
    HEADER_FILE_MIME_TYPE = 'Content-Type'
    HEADER_FILE_SIZE = 'Content-Length'
    HEADER_FILE_MD5_SUM = 'Storage-Service-File-MD5-Sum'
    HEADER_FILE_UPLOAD_DATE = 'Storage-Service-File-Upload-Date'
    HEADER_FILE_CACHE_CONTROL = 'Cache-Control'

    # @param [TriskelStorage::File] file
    def map_file_to_headers(file)
      {
        HEADER_FILE_UUID => file.uuid,
        HEADER_FILE_NAME => file.name,
        HEADER_FILE_LABELS => file.labels.join(','),
        HEADER_FILE_MIME_TYPE => file.mime_type,
        HEADER_FILE_SIZE => file.size.to_s,
        HEADER_FILE_CACHE_CONTROL => file.cache_control
      }
    end

    def map_metadata_to_file(data)
      file = TriskelStorage::File.new
      file.uuid = data['uuid']
      file.name = data['name']
      metadata = data['metadata']
      file.labels = metadata['labels']
      file.mime_type = metadata['mimeType'].split(';').first
      file.size = metadata['size']
      file.md5_sum = metadata['md5Sum']
      file.upload_date = Time.parse(metadata['uploadDate'])
      file.cache_control = metadata['cacheControl']
      file
    end

    def map_file(result)
      file = TriskelStorage::File.new
      file.uuid = result.headers[HEADER_FILE_UUID]
      file.name = result.headers[HEADER_FILE_NAME]
      file.mime_type = result.headers[HEADER_FILE_MIME_TYPE]
      file.size = result.headers[HEADER_FILE_SIZE]
      file.labels = result.headers[HEADER_FILE_LABELS].split(',')
      file.md5_sum = result.headers[HEADER_FILE_MD5_SUM]
      file.upload_date = Time.parse(result.headers[HEADER_FILE_UPLOAD_DATE])
      file.cache_control = result.headers[HEADER_FILE_CACHE_CONTROL]
      file.data = result.body
      file
    end
  end
end
