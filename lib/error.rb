# frozen_string_literal: true

module TriskelStorage
  # Exception when client cant find requested file.
  class FileNotFoundError < StandardError
    def initialize(uuid)
      super("Cannot find file #{uuid} at storage service")
    end
  end
end
