# frozen_string_literal: true

require 'httparty'
require_relative 'file'
require_relative 'mapper'
require_relative 'error'

module TriskelStorage
  # HTTP Storage Service client
  class HTTPStorage
    include HTTParty

    query_string_normalizer(proc { |query|
      query.map do |key, value|
        value.map { |v| "#{key}=#{v}" }
      end.join('&')
    })

    def initialize(base_url)
      @base_url = base_url
      @mapper = Mapper.new
    end

    STORAGE_ENDPOINT = 'storage'
    METADATA_ENDPOINT = 'metadata'

    # @param [TriskelStorage::File] file
    def save(file)
      self.class.post(url(@base_url, STORAGE_ENDPOINT),
                      headers: @mapper.map_file_to_headers(file),
                      body: file.data)
    end

    # @param [String]
    # @return [TriskelStorage::File]
    # @raise FileNotFoundError
    def retrieve(uuid)
      result = self.class.get(url(@base_url, STORAGE_ENDPOINT, uuid))
      check_not_found_response(result, uuid)
      @mapper.map_file(result)
    end

    def check_not_found_response(result, uuid)
      raise FileNotFoundError, uuid if result.body.nil? || result.body.empty? || result.response.code.to_i == 404
    end

    # @param [String]
    def delete_by_uuid(uuid)
      self.class.delete(url(@base_url, STORAGE_ENDPOINT, uuid))
    end

    # @param [String]
    # @return [TriskelStorage::File]

    def find_by_uuid(uuid)
      result = self.class.get(url(@base_url, METADATA_ENDPOINT, uuid))
      check_not_found_response(result, uuid)
      @mapper.map_metadata_to_file(result)
    end

    # @param [Array]
    # @return [Array][TriskelStorage::File]
    def find_by_labels(labels)
      result = find_by_labels_request(labels)
      results = []
      unless result.body.nil? || result.body.empty?
        result.each do |file|
          results.push @mapper.map_metadata_to_file(file)
        end
      end
      results
    end

    private

    def find_by_labels_request(labels)
      self.class.get(
        url(@base_url, METADATA_ENDPOINT),
        query: { labels: labels }
      )
    end

    def url(*parts)
      parts.join('/')
    end
  end
end
