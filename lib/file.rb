# frozen_string_literal: true

module TriskelStorage
  # File representation class.
  # Each uploaded/retrieved file must accomplish this interface
  class File
    attr_accessor :uuid
    attr_accessor :name
    attr_accessor :data

    def initialize
      @metadata = Metadata.new
    end

    def size
      @metadata.size
    end

    def size=(size)
      @metadata.size = size
    end

    def mime_type
      @metadata.mime_type
    end

    def mime_type=(mime_type)
      @metadata.mime_type = mime_type
    end

    def md5_sum
      @metadata.md5_sum
    end

    def md5_sum=(md5_sum)
      @metadata.md5_sum = md5_sum
    end

    def labels
      @metadata.labels
    end

    def labels=(labels)
      @metadata.labels = labels
    end

    def upload_date
      @metadata.upload_date
    end

    def upload_date=(upload_date)
      @metadata.upload_date = upload_date
    end

    def cache_control
      @metadata.cache_control
    end

    def cache_control=(cache_control)
      @metadata.cache_control = cache_control
    end

    # Metadata representation class
    class Metadata
      attr_accessor :size
      attr_accessor :mime_type
      attr_accessor :labels
      attr_accessor :upload_date
      attr_accessor :md5_sum
      attr_accessor :cache_control
    end
    private_constant :Metadata
  end
end
