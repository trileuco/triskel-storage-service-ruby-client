# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v2.2.1] - 2020-07-01

### Fixed

- [TSSTORAGE-50](https://redmine.trileuco.com/issues/TSSTORAGE-50)
  - Fix warning [DEPRECATION] HTTParty will no longer override response#nil?

## [Unreleased]

## [2.2.0] - 2019-10-06
### Changed
- Ruby version to from 2.0.0 to 2.3.1 (fixing rubocop offenses)

## [2.1.0] - 2019-09-26
### Removed
- MD5 sum on upload, is not required anymore

## [2.0.0] - 2019-08-22
### Changed
- Relax httparty gem version restrictions ([#2](https://bitbucket.org/trileuco/triskel-storage-service-ruby-client/pull-requests/2))
- BREAKING CHANGE: Rename the gem from storage_service_ruby_client to triskel_storage_service_ruby_client ([#4](https://bitbucket.org/trileuco/triskel-storage-service-ruby-client/pull-requests/4)). The main module of the gem has been changed also from TrileucoStorage to TriskelStorage.

## [1.0.0] - 2019-07-12
### Added
- Initial version of ruby client to access to features on the http endpoint of Triskel Storage Service
