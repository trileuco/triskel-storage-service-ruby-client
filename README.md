
# Storage service Ruby HTTP client
Client library for the storage service [project](https://bitbucket.org/trileuco/triskel-storage-service).

##  Requiring the library
Put this on your Gemfile to import the library. Be aware that you will need to add your SSH public key to the repository.
```ruby
gem 'triskel_storage_service_ruby_client', git: 'git@bitbucket.org:trileuco/triskel-storage-service-ruby-client.git', 
                                           branch: 'master'
```
More late in your code require the proper gem
```ruby
require 'triskel_storage_service_ruby_client'
```
## Invoking the client
```ruby
TriskelStorage::HTTPStorage.new('http://localhost:8080')
```
## Saving files
````ruby
file = TriskelStorage::File.new
file.uuid = 'A1234'
file.name = "test_file.txt"
file.data = "This is a test file."
file.labels = %w[my test file]
file.mime_type = 'text/plain'
file.size = file.data.bytesize
client.save(file)
````
## Retrieving files with payload
This will return the file and its binary data. You can take a look about all available file information on its [entity](lib/file.rb)
```ruby
file = client.retrieve('A1234')
```
## Query metadata of file
This will only return the file entities with the metadata associated. The content of th file is omitted.
```ruby
file = client.find_by_uuid('A1234')
```
## Search metadata of files by labels
```ruby
files = client.find_by_labels(%w[my test file])
```
## Delete a file
```ruby
client.delete_by_uuid('A1234')
```
## How to run tests
In the root of the project, type the following
```bash
rake test_units
```

## Code syntax and conventions checks

We use overcommit for automated git hooks that validate code syntax and conventions. To configure overcommit go to the project root and execute:

```bash
bundle install --gemfile=.overcommit_gems.rb
overcommit --install
```

Check that everything is working correctly with:

```bash
overcommit -r
```

Now before each commit the validations will be executed and if something isn't correct the commit won't be done.
