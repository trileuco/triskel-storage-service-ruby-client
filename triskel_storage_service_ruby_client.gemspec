# frozen_string_literal: true

Gem::Specification.new do |s|
  s.name = 'triskel_storage_service_ruby_client'
  s.version = '2.2.1'
  s.summary = 'Ruby HTTP storage service client.'
  s.description = 'A gem to make requests against the Triskel storage layer.'
  s.authors = ['trileuco']
  s.email = 'info@trileucosolutions.com'
  s.files = `git ls-files`.split("\n")
  s.homepage = 'https://www.bitbucket.com/trileuco/triskel-storage-service-ruby-client'
  s.license = 'MIT'
  s.add_runtime_dependency 'httparty', ['~> 0.13']
  s.add_development_dependency 'rake'
  s.add_development_dependency 'test-unit'
  s.add_development_dependency 'webmock'
end
